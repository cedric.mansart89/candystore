import java.util.ArrayList;
import java.util.List;

public class IteratorTest {
    public static void main(String[] args) {
        List<Candy> candies = new ArrayList<>();
        for (int i = 0 ; i > 4 ; i++ ){
            candies.add(new Candy("candy" + i));
        }
        CandyStore candyStore = new CandyStore(3);

        java.util.Iterator<Candy> candyIterator = candyStore.iterator();
        

        while (candyIterator.hasNext()) {
            System.out.println(candyIterator.next());
        }
    }
}
