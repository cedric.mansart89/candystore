import java.util.Iterator;

public class CandyStore {
    private Candy[] candies;

    public CandyStore(int size) {
        for (int i = 0; i < size; i++)
            candies[i] = new Candy("candy" + i);
    }

    public Iterator iterator() {
        return new CandyStoreIterator(candies);
    }
}
