public class Candy {
    private String name;

    public Candy(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Candy{" + "name=" + name + "}";
    }
}