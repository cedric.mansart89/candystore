public class CandyStoreIterator implements Iterator {

    private Candy[] candies;
    private int position;

    public CandyStoreIterator(Candy[] candies){
        this.candies = candies;
        this.position = 0;
    }

    @Override
    public boolean hasNext() {
        return this.position < this.candies.length;
    }

    @Override
    public Object next() {
        Candy candy = this.candies[this.position];
        this.position++;
        return candy;
    }
}
